package com.example.Maven_TestcaseAI_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MavenTestcaseAiDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MavenTestcaseAiDemoApplication.class, args);
	}

}
