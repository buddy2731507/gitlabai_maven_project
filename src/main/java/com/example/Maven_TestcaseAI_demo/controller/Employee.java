//write a program to mention all employees details and mapping it for maven project
package com.example.Maven_TestcaseAI_demo.controller;
import java.util.ArrayList;
import java.util.List;


class Employee {

    private String name;
    private int age;
    private double salary;

    public Employee(String name, int age, double salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public double getSalary() {
        return salary;
    }
}

class Database {
    private List<Employee> employees;

    public Database() {
        employees = new ArrayList<>();
    }

    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

    public void printAllEmployees() {
        for (Employee e : employees) {
            System.out.println("Name: " + e.getName()); 
            System.out.println("Age: " + e.getAge());
            System.out.println("Salary: " + e.getSalary());
            System.out.println();
        }
    }
}
